# Trustful voting app deployment

TODO: Kustomize 

Deployment is via GitOps(Pull-based), edit the manifest 'image version' in [trustful-vote-app-production.yaml](./manifests/production/trustful-vote-app-production.yaml) to trigger change

| Deployment | fields |
| --- | --- |
| result | registry.gitlab.com/kkk1204313/abc/technology/trustful-voting-app/result:latest |
| vote | registry.gitlab.com/kkk1204313/abc/technology/trustful-voting-app/vote:latest |
| worker | registry.gitlab.com/kkk1204313/abc/technology/trustful-voting-app/worker:latest |


# References
The ultimate guide to GitOps with GitLab [[1]]  
GitOps with GitLab: Connecting GitLab with a Kubernetes cluster for GitOps-style application delivery [[2]]  


[1]:https://about.gitlab.com/blog/2022/04/07/the-ultimate-guide-to-gitops-with-gitlab
[2]:https://about.gitlab.com/blog/2022/03/21/gitops-with-gitlab/